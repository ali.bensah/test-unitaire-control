<?php

namespace Client;

class StringFormatter
{
  public function __construct()
  {

  }
    /**
     * @return string
     */
   public function concat($str1, $str2)
    {
        return $str1.$str2;
    }

    /**
     * @return string
     */
   public function toCamelCase($str1, $str2)
    {
      $str1 = ucfirst($str1);
      $str2 = ucfirst($str2);

      return $str1.$str2;

    }

    /**
     * @return string
     */
   public function prefix($string, $prefix, $camel)
    {
      if($camel){
          return $this->toCamelCase($prefix,$string);
      } else {
          return $this->concat($prefix,$string);
      }
    }

    /**
     * @return string
     */
   public function suffix($string, $suffix, $camel)
    {
      if($camel){
          return $this->toCamelCase($string, $suffix);
      } else {
          return $this->concat($string, $suffix);
      }
    }
}
